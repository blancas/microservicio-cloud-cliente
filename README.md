# microservicio-cloud-cliente

[Config Server – Client](https://howtodoinjava.com/spring-cloud/spring-cloud-config-server-git/)

Config Server – Client Side Configuration
Now we will proceed to the client side implementation where we will use those properties from a separate microservice which is our final goal – to externalize the configuration to different service.

Create Maven Project
Go to https://start.spring.io/ web portal and generate client project with the below selected artifacts:

Actuator
Config Client
Web
Rest Repositories
The screen will look like below before generation, once we click on generate, we will get the .zip file download option. Like Spring-Config-Server, unzip the file in some directory and import in eclipse.


Create REST Resource
Add one RestController to view the Server side property values in the response. To do that open the @SpringBootApplication class file that has been generated, and add the below small class in the end of that file. This is very simple and straight forward, we are just exposing one method at /message URL where we will just return the property value of msg that will be supplied by the config server microservice, which is configured to a local git repository (which will be migrated to a remote git repository in production!).



Bind with the Config Server
Create one file called bootstrap.properties in the src\main\resources directory and add the below properties to connect with the config server along with some required configuration.

spring.application.name=config-server-client
 
#Active Profile - will relate to development properties file in the server.
#If this property is absent then default profile will be activated which is
#the property file without any environment name at the end.
 
spring.profiles.active=development
 
# this is the default
 
spring.cloud.config.uri=http://localhost:8888
 
management.security.enabled=false
Let’s understand the properties now.

spring.application.name is just the application name of the microservice that would be deployed.
spring.cloud.config.uri is the property to mention the config server url. Point to note that our config server is running on port 8888; verify it by opening the application.properties file of the spring config server code base and check the server.port=8888.
management.security.enabled=false will disable the spring security on the management endpoints like /env, /refresh etc. This is for development settings, in production security should be enabled.
Verify Client Config
This is very much we need to do in the config client side, not do a final mvn clean install command on this project so that everything gets compiled properly and packaged also in the target folder as well as in local maven repository. We will start the config client service along with the server side and we will finally test the feature.


Demo
Lets test the config server application.

Build and Run Config Server Project
Open command prompt from spring-config-server folder and run mvn clean install command. Once build is completed run the application from that command prompt itself by java -jar command like java -jar target\spring-config-server-0.0.1-SNAPSHOT.jar.

This will start the config server service in 8888 port in localhost.

Build and Run Config Client Project
Similarly, Open command prompt from spring-config-client folder and run mvn clean install command. Once build is completed run the application from that command prompt itself by java -jar command like java -jar target\spring-config-client-0.0.1-SNAPSHOT.jar.

This will start the Config Client service in 8080 port of localhost.

Test REST Endpoint
Now in the browser open the /msg rest endpoint by browsing the url http://localhost:8080/msg. It should return Hello world - this is from config server which is mentioned in the config-server-client-development.properties file.